﻿using Dalamud.Game.ClientState.Objects.Types;

namespace AdrenalinePlugin.Service.Network {
	public static class ActorControl {
		public static void ActorControlTarget(dynamic packet, BattleChara? chara) {
			var category = (ushort)packet.category;
			if (category != 502) {
				return;
			}

			uint actorId = packet.param2;
			if (actorId == AdrenalinePlugin.EnvId) {
				actorId = 0U;
			}

			// TODO: figure it out
			// var newSign = c.sign.value === ctrl.param1 ? null : ctrl.param1;
			// chara.
			// if (newSign != null) {
			// 	const oldC = this.parser.combatants.value.find(c => c.sign.value === newSign);
			// 	if (oldC) {
			// 		oldC.sign.next(null);
			// 	}
			// }
			// c.sign.next(c.sign.value === ctrl.param1 ? null : ctrl.param1);
			//
			// uint targetId = actorControlTarget.TargetID;
			// byte signId = (byte) (actorControlTarget.param1 & (uint) byte.MaxValue);
			// this._markerManager.UpdateSign(packetDate, actorId, targetId, signId);
		}

		public static void ActorControlSelf(dynamic packet, BattleChara? chara) {
			var timestamp = AdrenalinePlugin.Timestamp();
			var category = (ushort)packet.category;
			if (category == 109) {
				if ((int)packet.param2 != 0) {
					var ll = new object?[] {
						timestamp, packet.param1, packet.param2,
						packet.param3, packet.param4,
						packet.param5, packet.param6,
					};

					AdrenalinePlugin.processor.PushLogLine(33, ll);
				}
			}
			else if (category == 505) {
				var limitBreakLogLine = new object?[] {
					timestamp, ((uint)packet.param1).ToString("X"), ((uint)packet.param2).ToString("X"),
				};
				AdrenalinePlugin.processor.PushLogLine(36, limitBreakLogLine);
			}
		}

		public static void ActorControlGeneric(dynamic packet, BattleChara? chara) {
			var timestamp = AdrenalinePlugin.Timestamp();
			var category = (ushort)packet.category;
			switch (category) {
				case 23:
					var which = (uint)packet.param2 == 4 ? "HoT" : "DoT";
					var overTimeLogLine = new object?[] {
						timestamp, chara?.ObjectId.ToString("X8"), chara?.Name, which,
						packet.param1.ToString("X"), packet.param3.ToString("X"),
						chara?.CurrentHp, chara?.MaxHp,
						chara?.CurrentMp, 10000,
						null, null,
						chara?.Position.X, chara?.Position.Z, chara?.Position.Y, chara?.Rotation
					};

					AdrenalinePlugin.processor.PushLogLine(24, overTimeLogLine);
					break;
				case 15:
					var action = AdrenalinePlugin.actionSheet.GetRow((uint)packet.param3);
					// TODO: Not sure about this
					var reason = packet.param4 == 0 ? "Cancelled" : "Interrupted";
					var networkCancelMessageLogLine = new object?[] {
						timestamp, chara?.ObjectId.ToString("X8"), chara?.Name,
						action?.RowId.ToString("X"), action?.Name, reason
					};
					AdrenalinePlugin.processor.PushLogLine(23, networkCancelMessageLogLine);
					break;
				case 6:
					var source = AdrenalinePlugin.objectTable.SearchById((uint)packet.param1);
					var networkDeathLogLine = new object?[] {
						timestamp, chara?.ObjectId.ToString("X8"), chara?.Name,
						source?.ObjectId.ToString("X8"), source?.Name
					};
					AdrenalinePlugin.processor.PushLogLine(25, networkDeathLogLine);
					break;
				case 34:
					var wot = 0; //packet.MessageHeader.Unknown4;
					var networkTargetIconLogLine = new object?[] {
						timestamp, chara?.ObjectId.ToString("X8"), chara?.Name,
						wot, packet.padding,
						packet.param1, packet.param2,
						packet.param3, packet.param4
					};
					AdrenalinePlugin.processor.PushLogLine(27, networkTargetIconLogLine);
					break;
				case 22:
					var statusId = (uint)packet.param1;
					var duration = (uint)packet.param2;
					var appliedBy = (uint)packet.param3;
					var statusCount = (ushort)packet.param4;
					// Hidden statuses
					if (appliedBy == 1) {
						return;
					}
					if (appliedBy == 0 || appliedBy == AdrenalinePlugin.EnvId) {
						appliedBy = packet.targetActorId;
					}

					var index = (byte)packet.param1;
					StatusManager.AddUpdateBuffForTarget(
						appliedBy, 
						chara, 
						statusId, 
						statusCount, 
						0.0f,
						index,
						true
					);
					break;
				case 54:
					// uint actorId2 = packet.MessageHeader.ActorID;
					// Combatant combatantById3 = this._combatantManager.GetCombatantById(actorId2);
					// string message = this._logFormat.FormatNetworkTargettableMessage(actorId1, chara?.Name, actorId2,
					// 	combatantById3?.Name, (byte)(packet.param1 & (uint)byte.MaxValue));
					// this._logOutput.WriteLine((LogMessageType)34, packetDate, message);
					// this._dataEvent.OnParsedLogLine((LogMessageType)34, message);
					break;
				case 35:
					var target = AdrenalinePlugin.objectTable.SearchById((uint)packet.param3);
					var networkTetherLogLine = new object?[] {
						timestamp, chara?.ObjectId.ToString("X8"), chara?.Name,
						target?.ObjectId.ToString("X8"), target?.Name,
						packet.padding,
						packet.param1, packet.param2,
						packet.param3, packet.param4,
						packet.padding1
					};

					AdrenalinePlugin.processor.PushLogLine(35, networkTetherLogLine);
					break;
			}
		}
	}
}