﻿using System;
using System.Collections.Generic;
using Dalamud.Game.ClientState.Objects.Types;
using FFXIV_ACT_Plugin.Common.Models;

namespace AdrenalinePlugin.Service.Network {
	public static class StatusList {
		public static void StatusEffectList(dynamic packet, BattleChara? chara) {
			var num = (uint)(
				(byte)packet.jobId +
				((byte)packet.level1 << 8) +
				((byte)packet.level2 << 16) +
				((byte)packet.level3 << 24)
			);

			var timestamp = AdrenalinePlugin.Timestamp();
			var logLine = new object?[] {
				timestamp, chara?.ObjectId.ToString("X"), chara?.Name,
				num.ToString("X8"),
				packet.hp, packet.hpMax, 
				packet.mana, packet.manaMax,
				packet.damageShield, null,
				chara?.Position.X, chara?.Position.Z, chara?.Position.Y, chara?.Rotation,
				packet.unknown1.ToString("X"), packet.unknown2.ToString("X")
			};

			AdrenalinePlugin.processor.PushLogLine(38, logLine);

			var buffs = new List<NuStatus>();
			for (byte index = 0; index < 30; ++index) {
				var effect = packet.effectsRaw[index];

				if (effect.effectId == 0) {
					continue;
				}

				buffs.Add(new NuStatus {
					statusId = effect.effectId,
					sourceId = effect.sourceActorId,
					duration = Math.Abs(effect.duration),
					stackCount = effect.unknown1
				});

				// if (buffs[index].BuffID == 0) {
				// 	continue;
				// }
				//
				// buffs[index].BuffExtra = effect.unknown1;
				// buffs[index].ActorID = effect.sourceActorId;
				// buffs[index].Duration = Math.Abs(effect.duration);
				// buffs[index].Timestamp = packetDate;
			}

			StatusManager.ReplaceAllBuffsForTarget(chara, buffs);
		}

		public static void StatusEffectList3(dynamic packet, BattleChara? chara) {
			var logLine = new List<object?> {
				AdrenalinePlugin.Timestamp(), chara?.ObjectId, chara?.Name
			};
			AdrenalinePlugin.processor.PushLogLine(42, logLine);

			var buffs = new List<NuStatus>();
			for (byte index = 0; index < 30; ++index) {
				var effect = packet.effectsRaw[index];

				if (effect.effectId == 0) {
					continue;
				}

				buffs.Add(new NuStatus {
					statusId = effect.effectId,
					sourceId = effect.sourceActorId,
					duration = Math.Abs(effect.duration),
					stackCount = effect.unknown1
				});

				// if (buffs[index].BuffID == 0) {
				// 	continue;
				// }
				//
				// buffs[index].BuffExtra = effect.unknown1;
				// buffs[index].ActorID = effect.sourceActorId;
				// buffs[index].Duration = Math.Abs(effect.duration);
				// buffs[index].Timestamp = packetDate;
			}

			StatusManager.ReplaceAllBuffsForTarget(chara, buffs);
		}
	}
}