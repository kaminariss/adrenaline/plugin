﻿using System.Collections.Generic;
using Dalamud.Plugin.Ipc;

namespace AdrenalinePlugin.Service {
	public static class PartyListener {
		internal static ICallGateSubscriber<uint, List<object>, bool> networkSub = null!;

		public static void Initialize() {
			networkSub = AdrenalinePlugin.pluginInterface.GetIpcSubscriber<uint, List<object>, bool>("NextUI.PartyChanged");

			networkSub.Subscribe(PartyChanged);
			AdrenalinePlugin.SubscribeIpcEvent("NetworkEvent");
		}

		public static void PartyChanged(
			uint partyLeader, List<object> party
		) {
			var ll = new List<object?> {
				AdrenalinePlugin.Timestamp(),
				party.Count
			};
			
			foreach (dynamic member in party) {
				ll.Add(((uint)member.id).ToString("X"));
			}
			AdrenalinePlugin.processor.PushLogLine(11, ll);
		}

		public static void Shutdown() {
			networkSub.Unsubscribe(PartyChanged);
		}
	}
}