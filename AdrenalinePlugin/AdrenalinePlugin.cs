﻿using System;
using System.Collections.Generic;
using Dalamud.Data;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Conditions;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Game.Gui;
using Dalamud.Game.Text;
using Dalamud.Game.Text.SeStringHandling;
using Dalamud.IoC;
using Dalamud.Plugin;
using Dalamud.Plugin.Ipc;
using Lumina.Excel;
using AdrenalinePlugin.Model;
using AdrenalinePlugin.Service;
using TerritoryType = Lumina.Excel.GeneratedSheets.TerritoryType;
using Action = Lumina.Excel.GeneratedSheets.Action;
using Status = Lumina.Excel.GeneratedSheets.Status;

namespace AdrenalinePlugin {
	// ReSharper disable once ClassNeverInstantiated.Global
	// ReSharper disable once InconsistentNaming
	public class AdrenalinePlugin : IDalamudPlugin {
		public const uint EnvId = 0xE0000000;

		public string Name => "AdrenalinePlugin";

		/** Dalamud injected services */
		// ReSharper disable InconsistentNaming
		// ReSharper disable ReplaceAutoPropertyWithComputedProperty
		[PluginService] public static DalamudPluginInterface pluginInterface { get; set; } = null!;

		[PluginService] public static ClientState clientState { get; set; } = null!;
		[PluginService] public static DataManager dataManager { get; set; } = null!;
		[PluginService] public static ObjectTable objectTable { get; set; } = null!;
		[PluginService] public static Framework framework { get; set; } = null!;
		[PluginService] public static Condition condition { get; set; } = null!;

		[PluginService] public static ChatGui chatGui { get; set; } = null!;
		// ReSharper enable InconsistentNaming
		// ReSharper enable ReplaceAutoPropertyWithComputedProperty

		public static ParseProcessor processor = null!;
		public static DpsProcessor dps = null!;
		public static bool inCombat;

		public static ExcelSheet<TerritoryType> territorySheet = null!;
		public static ExcelSheet<Action> actionSheet = null!;
		public static ExcelSheet<Status> statusSheet = null!;

		public static string Timestamp() => DateTime.UtcNow.ToString("O");

		protected static ICallGateSubscriber<string, string, bool> ipcSubscriber = null!;
		protected static ICallGateSubscriber<string, string, object, bool> ipcBroadcaster = null!;

		public AdrenalinePlugin() {
			territorySheet = dataManager.GetExcelSheet<TerritoryType>();
			actionSheet = dataManager.GetExcelSheet<Action>();
			statusSheet = dataManager.GetExcelSheet<Status>();

			ipcSubscriber = pluginInterface.GetIpcSubscriber<string, string, bool>("NextUI.Register");
			ipcBroadcaster = pluginInterface.GetIpcSubscriber<string, string, object, bool>("NextUI.Broadcast");

			processor = new ParseProcessor();
			dps = new DpsProcessor(processor);
			dps.Tick += DpsOnTick;

			NetworkListener.Initialize();
			PartyListener.Initialize();
			CombatantManager.Initialize();

			clientState.TerritoryChanged += ClientStateOnTerritoryChanged;
			chatGui.ChatMessage += ChatGuiOnChatMessage;
			framework.Update += FrameworkOnUpdate;

			CheckPlayer();
		}

		protected void DpsOnTick(List<UnitDps> obj) {
			ipcBroadcaster.InvokeFunc("AdrenalinePlugin", "dps", obj);
		}

		protected static void FrameworkOnUpdate(Framework framework1) {
			var currentlyInCombat = condition[ConditionFlag.InCombat];
			if (inCombat == currentlyInCombat) {
				return;
			}

			inCombat = currentlyInCombat;
			dps.CombatStateChanged(inCombat);
		}

		public static bool SubscribeIpcEvent(string eventName) {
			return ipcSubscriber.InvokeFunc("AdrenalinePlugin", eventName);
		}

		protected static void ChatGuiOnChatMessage(
			XivChatType type,
			uint senderId,
			ref SeString sender,
			ref SeString message,
			ref bool isHandled
		) {
			var ll = new object?[] { Timestamp(), type.ToString("X"), sender, message };
			processor.PushLogLine(00, ll);
		}

		protected static void ClientStateOnTerritoryChanged(object? sender, ushort e) {
			var territory = territorySheet.GetRow(e);
			var territoryName = territory?.Map?.Value?.PlaceName?.Value?.Name;
			var ll = new object?[] { Timestamp(), e.ToString("X"), territoryName };
			processor.PushLogLine(01, ll);

			PrimaryPlayer();
			
			CombatantManager.CheckAll();
		}

		protected static void PrimaryPlayer() {
			var player = clientState.LocalPlayer;
			if (player == null) {
				return;
			}

			var ll = new object[] { Timestamp(), player.ObjectId.ToString("X8"), player.Name };
			processor.PushLogLine(02, ll);
		}

		protected void CheckPlayer() {
			var player = clientState.LocalPlayer;
			if (player == null) {
				return;
			}

			ClientStateOnTerritoryChanged(this, clientState.TerritoryType);
		}

		public void Dispose() {
			clientState.TerritoryChanged -= ClientStateOnTerritoryChanged;
			chatGui.ChatMessage -= ChatGuiOnChatMessage;
			framework.Update -= FrameworkOnUpdate;

			NetworkListener.Shutdown();
			PartyListener.Shutdown();
			CombatantManager.Shutdown();

			dps.Tick -= DpsOnTick;
			dps.Dispose();

			GC.SuppressFinalize(this);
		}
	}
}